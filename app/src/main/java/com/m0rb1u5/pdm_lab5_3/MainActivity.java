package com.m0rb1u5.pdm_lab5_3;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
   private TextView textView;
   private GridView gridView;
   private Comida[] datos = new Comida[] {
         new Comida("arroz con pollo", "pollo, cebolla, ajo, ají amarillo, ají panca, culantro, arroz, ...", 45),
         new Comida("ají de gallina", "gallina, pan, cebolla, ajo, ají amarillo, leche, papas, ...", 20),
         new Comida("lomo saltado", "carne de res, cebolla, tomates, ají amarillo, ajo, sillao, ...", 30),
         new Comida("tacu tacu", "frejol canario, manteca de chancho, arroz, cebolla, ajo, ...", 105),
         new Comida("ceviche", "pescado fresco, limones, cebolla, ají limo, culantro, sal, ...", 10)
   };
   private AdaptadorComidas adapter;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);

      adapter = new AdaptadorComidas(this, datos);
      gridView = (GridView) findViewById(R.id.gridView);
      textView = (TextView) findViewById(R.id.textView);
      gridView.setAdapter(adapter);
      gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
         @Override
         public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Comida comida = (Comida) parent.getItemAtPosition(position);
            textView.setText("Opción seleccionada: " + comida.getNombre());
         }
      });
      registerForContextMenu(textView);
   }

   @Override
   public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
      super.onCreateContextMenu(menu, v, menuInfo);
      MenuInflater inflater = getMenuInflater();
      inflater.inflate(R.menu.menu_contextual, menu);
   }

   @Override
   public boolean onContextItemSelected(MenuItem item) {
      switch (item.getItemId()) {
         case R.id.toast:
            Toast.makeText(this,"Este toast es lanzado a partir de un menú contextual", Toast.LENGTH_LONG).show();
            return true;

         case R.id.dialog:
            FragmentManager fragmentManager = getFragmentManager();
            DialogoInfo dialogoInfo = new DialogoInfo();
            dialogoInfo.show(fragmentManager, "tagInfo");
            return true;

         default:
            return super.onContextItemSelected(item);
      }
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_main, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      //noinspection SimplifiableIfStatement
      switch (id) {
         case R.id.action_settings:
            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
            startActivity(intent);
            return true;

         case R.id.acercade:
            FragmentManager fragmentManager = getFragmentManager();
            DialogoInfo dialogoInfo = new DialogoInfo();
            dialogoInfo.show(fragmentManager, "tagInfo");
            return true;

         case R.id.salir:
            finish();
            return true;

         default:
            return super.onOptionsItemSelected(item);
      }
   }

   public static class DialogoInfo extends DialogFragment {
      @Override
      public Dialog onCreateDialog(Bundle savedInstanceState) {
         AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
         builder.setMessage("Juan Carlos Carbajal Ipenza\nUniversidad Nacional de Ingeniería")
               .setTitle("Información")
               .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                     dialog.cancel();
                  }
               });
         return builder.create();
      }
   }

   class AdaptadorComidas extends ArrayAdapter<Comida> {
      public AdaptadorComidas(Context context, Comida[] datos) {
         super(context, R.layout.listitem_comida, datos);
      }

      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
         LayoutInflater layoutInflater = LayoutInflater.from(getContext());
         View item = layoutInflater.inflate(R.layout.listitem_comida, null);

         TextView nombre = (TextView) item.findViewById(R.id.nombre);
         nombre.setText(datos[position].getNombre());

         TextView tiempoCoccion = (TextView) item.findViewById(R.id.tiempoCoccion);
         tiempoCoccion.setText(datos[position].getTiempoCoccion());

         TextView ingredientes = (TextView) item.findViewById(R.id.ingredientes);
         ingredientes.setText(datos[position].getIngredientes());

         return item;
      }
   }
}
