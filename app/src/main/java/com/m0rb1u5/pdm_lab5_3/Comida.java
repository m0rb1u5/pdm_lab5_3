package com.m0rb1u5.pdm_lab5_3;

/**
 * Created by m0rb1u5 on 30/01/16.
 */
public class Comida {
   private String nombre;
   private String ingredientes;
   private int tiempoCoccion;

   public Comida(String nombre, String ingredientes, int tiempoCoccion) {
      setIngredientes(ingredientes);
      setNombre(nombre);
      setTiempoCoccion(tiempoCoccion);
   }

   public String getIngredientes() {
      return ingredientes;
   }

   public void setIngredientes(String ingredientes) {
      this.ingredientes = ingredientes;
   }

   public String getNombre() {
      return nombre;
   }

   public void setNombre(String nombre) {
      this.nombre = nombre;
   }

   public String getTiempoCoccion() {
      return tiempoCoccion + " m";
   }

   public void setTiempoCoccion(int tiempoCoccion) {
      this.tiempoCoccion = tiempoCoccion;
   }
}
